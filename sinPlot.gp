res
set term postscript eps enh col "Helvetica" 22
set out "myLittleFigure.eps"
set enc iso_8859_1

#--------------------------------------------------------
col_orange = "#fc8d59"
col_blue   = "#2b83ba"
col_green  = "#31a354"
col_purp   = "#756bb1"
#------------
set style incr user
LW=8
LS=0
PS = 0.8*2.4


LS=LS+1 ; set style line LS lt 1 pt  4 ps PS lw LW lc rgb col_orange
LW=4
LS=LS+1 ; set style line LS lt 1 pt  6 ps PS lw LW lc rgb col_blue
#----

#--------------------------------------------------------
set xla "{/=24 Radians}"
set xtics ("0" 0,"0.5{/Symbol p}" pi/2, "{/Symbol p}" pi, \
	"1.5{/Symbol p}" 1.5*pi, "2{/Symbol p}" 2*pi )
set xra[0:2*pi]
#rita en horistontell linje
set xzeroaxis
#----
set yla "arb. units"
set yti -1,1,1
set yra [-1:1]
#----
set key left center at gr 0.6,0.89



p \
   "< grep -v e2 sinData | sort" u 1:2 t "datafile" w lp,\
     sin(x) w l t "sin(x)"
