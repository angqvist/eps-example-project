eps example project:
--------------------
Generating an eps figure is as simple as:

```
gnuplot sinPlot.gp
```

This will generate an [eps file](myLittleFigure.eps). Which cannot currently be viewed directly in  GitLab.

A workaround is to produce a pdf file from the eps with:

```
epstopdf myLittleFigure.eps
```

Which creates this [pdf file](myLittleFigure.pdf) which can be viewed in GitLab!


It would however be nice to not do this workaround and view eps figures directly.

